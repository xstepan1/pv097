---
lang: en-US
documentclass: scrreprt
papersize: a4
fontsize: 12pt
mainfont: TeX Gyre Pagella
sansfont: Lato
monofont: Consolas
title: CodePizza
subtitle: A Helveg Feature
author: Adam Štěpánek
date: Spring 2023
header-includes:
    - \definecolor{vulkan}{HTML}{ac162c}
    - \dedication{\normalsize\textit{Dedicated to the memory of Pizza Nostra.}}
    - \addtokomafont{author}{\normalsize}
    - \addtokomafont{date}{\small}
linkcolor: vulkan
filecolor: vulkan
citecolor: vulkan
urlcolor: vulkan
toccolor: vulkan
urlstyle: tt
...

## Introduction

Hello, and again, welcome to _CodePizza_. CodePizza is *not a standalone tool* but a new feature of Helveg, a software visualization tool. CodePizza turns your C# codebases into pizzas like the one below!

Under the hood it uses a force-directed graph drawing algorithm to lay out the graph nodes in space, and simplex noise to generate the pizza-dough-ish texture.

![Sample CodePizza](../01_sample.png)

## Installation and Code Analysis

Helveg is a command-line tool that outputs single-file web applications. However, since this project is about CodePizza, I provide several ways of using Helveg to get to CodePizza easily.

You have three options:

### Prepared Samples

Open one of the sample single-file apps in the `bin` directory and avoid analyzing a codebase altogether. The samples are: `helveg-itself.html`, `immersive-reading.html` ([Immersive Reading in VR](https://is.muni.cz/th/vg2lf/)), `kafe.html` ([KAFE](https://gitlab.fi.muni.cz/lemma/kafe)), `diagnostics.html` (a sample with fire and smoke), and `clean.html` (a demo without diagnostics).

### Provided build of Helveg

Alternatively, you can use the provided `helveg.exe` to analyze your own C# codebase. This executable requires you to have .NET 6 SDK or newer installed.

To analyze a codebase run `helveg.exe` inside a directory with a C# project file or Visual Studio Solution file, or run:

```bat
helveg.exe ./my-repo
```

to analyze a codebase in `./my-repo`. Once the analysis is done, the tool outputs a single-file web app similar to the provided samples.

The CLI features a number of parameters that could be interesting to you. For their complete list [see Helveg docs](https://helveg.net/docs/user-guide/cli-options/).

### Install Helveg from NuGet

Finally, to install the newest version of Helveg or to use it on Linux, run:

```bash
dotnet tool install -g helveg
```

Please note that this approach also requires you to have .NET 6 (or newer) installed on your machine.

If you have your .NET tools in a location accessible from `Path`, Helveg is used in the same was as the self-contained one above.

## CodePizza Activation and Configuration

Once you open one of Helveg's web apps, you may notice that _CodePizza_ is not active by default. To activate it, use the _IsEnabled_ checkbox in the _CodePizza_ subpanel of the _Appearance_ panel as show below (1).

The _Appearance_ panel also contains CodePizza's parameters _CrustWidth_ and _SauceWidth_, which influence the size of the resulting pizza dough.

![CodePizza controls in Helveg's UI](../02_ui.png)

Moreover, the _C# CodePizza Toppings_ subpanel (2) contains the mapping between C# concepts (e.g. classes and methods) and pizza ingredients (e.g. cheese and ham). This subpanel allows you to change this mapping as you see fit. If, for example, you don't like the fact that classes are by default represented by pineapples, click the _Class_ box and pick something else, like a pickle.

## Helveg Basics

This document won't delve into Helveg's controls and usage. That's what [Helveg's User Guide](https://helveg.net/docs/user-guide/) is for. Nevertheless, in short there are several Panels in Helveg's Dock:

**Data Panel**: Allows you to search and filter the analyzed date.

**Layout Panel**: Contains controls for layouting algorithms. Use this to position nodes of the graph.

**Appearance Panel**: Controls the appearance of the nodes and the diagram overall. Also contains CodePizza's parameters.

**Tools**: Settings of tools that are available in the upper right corner of the screen. Use them to move, cut, or collapse parts of the diagram, or to view properties of a specific node.

**Properties**: Shows properties of the currently selected node. Use the _Show Properties_ tool to select a node.

**Document**: Has basic metadata about the visualization. Also allows you to import or export settings and to capture static PNGs. Please note that state exported from the app is compatible only with that specific app.

**Guide**: A cheat sheet of sorts. And it also contains information about Helveg itself and its licensing.

## Meaning of the Visualization

The structural diagram dominating Helveg's viewport shows a bird's eye view of a C# codebase. Each node represents a specific entity: C# project, type, method, field, etc. The edges between nodes represent the relationships between entities, like inheritance, composition, `override`s, etc.

There are also effects. If a node is on fire, it means that a compiler error has been issued somewhere in it. Click the node with the _Show Properties_ tool to see the error message. Analogically, if a column of smooke is coming from a node, there is a warning attached to that node.

## TL;DR

1. Open either a prepared sample or use the CLI to analyze a C# codebase.
2. Enable _CodePizza_ in the _Appearance_ panel.
3. Modify the mapping between C# concepts and pizza ingredients if you want.
4. Run _ForceAtlas2_ to produce a more packed, pizza-like layout.
5. Save the current config or the current view using the _Document_ panel.
6. Share the output with friends or colleagues.


## Known Issues

- When zoomed in, the pizza dough and sauce can "shrink". This is a limitation of the `POINTS` rendering mode in WebGL.
- The sauce background "twitches" while zooming or when the browser is zoomed in. This is a limitation/bug caused by a missing API/docs of Sigma.js.
- The resolution of the toppings is 64x64 or 128x128 on purpose. Since these images are included in the output HTML files in base64, they must be kept small to keep the overall file size under control.
- Due to a bug in GitVersion, Helveg must be compiled in a git repo. This is why the attached source code is in a _dummy_ repository. Do not remove the `.git` directory if you want to compile Helveg from source.

## About 

This project was created as a final project to the _PV097 Visual Creativity Informatics_ course taught at the Faculty of Informatics, Masaryk University, in Spring of 2023.

The source code should be available along with this document or at [https://gitlab.com/helveg/helveg](https://gitlab.com/helveg/helveg), or at [https://github.com/cafour/helveg](https://github.com/cafour/helveg). It is licensed under the [3-Clause BSD](https://opensource.org/license/bsd-3-clause/) license. [A different license](https://www.microsoft.com/en-us/download/details.aspx?id=35825) applies to the Visual Studio icons.

The toppings have been created by modifing images by:

* [Sydney Troxell](https://www.pexels.com/@sydney-troxell-223521/)
* [Vincent Rivaud](https://www.pexels.com/@vince/)
* [Laker](https://www.pexels.com/@laker/)
* [Dominika Roseclay](https://www.pexels.com/@punchbrandstock/)
* [Ana Palade](https://www.pexels.com/@ana-palade-470459090/)
* [Brigitte Tohm](https://www.pexels.com/@brigitte-tohm-36757/)
* [Shameel mukkath](https://www.pexels.com/@shameel-mukkath-3421394/)
* [Polina Tankilevitch](https://www.pexels.com/@polina-tankilevitch/)
* [Pixabay](https://www.pexels.com/@pixabay/)
* [Chmee2](https://commons.wikimedia.org/wiki/File:Olomouck%C3%A9_tvar%C5%AF%C5%BEky_(3).jpg#/media/File:Olomoucké_tvarůžky_(3).jpg)
* [Manuel Mouzo](https://www.pexels.com/@manuel-mouzo-301044570/)
* [Pablo Macedo](https://www.pexels.com/@pablo-macedo-287472/)
* [Elizabeth Lizzie](https://www.pexels.com/@owayn/)
* [Cats Coming](https://www.pexels.com/@catscoming/)
* [Augustinus Martinus Noppé](https://www.pexels.com/photo/a-person-holding-a-slice-of-pizza-on-a-wooden-board-14334060/)
* [Beqa Tefnadze](https://www.pexels.com/@beqa-tefnadze-264627/)
* [Игорь Лушницкий](https://www.pexels.com/@igorlufoto/)
* [Andre saddi](https://www.pexels.com/@andre-saddi-292691575/)
* [Rene Strgar](https://www.pexels.com/@renestrgar/)
