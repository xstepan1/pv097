---
lang: en-US
documentclass: scrreprt
papersize: a4
fontsize: 12pt
author: Adam Štěpánek
date: 2023-05-07
title: "Helveg: CodePizza"
header-includes:
- \let\maketitle\relax
- \definecolor{vulkan}{HTML}{ac162c}
mainfont: TeX Gyre Pagella
linkcolor: vulkan
filecolor: vulkan
citecolor: vulkan
urlcolor: vulkan
toccolor: vulkan
...

**Name**: CodePizza

**Author**: Adam Štěpánek

**Semester**: Spring 2023

**General topic**: Software visualization / digital collage

**Basic math concept**: Simplex noise & force-directed graph layout

**Parameters**:

- 1x visualized C# repository (along with Helveg's parameters)
- 24x mappings between C# entities (e.g. classes, methods, etc) and pizza toppings (e.g. ham, cheese, etc.)
- 1x pizza crust width
- 1x pizza sauce width
- 1x force-directed layout - number of iterations of ForceAtlas2

**Format**: The CLI outputs a single-file web app, which in turn outputs PNGs. Maximum resolution depends on HW and browser support.

**Used tools**:

- Languages: C#, TypeScript, Sass, GLSL
- Libraries: [Helveg](https://helveg.net/), [Sigma.js](https://sigmajs.org/)
- Tools: Visual Studio Code, Photoshop

**Dependencies**:

- .NET 6 SDK (newer may run as well).
- Up-to-date web browser.
