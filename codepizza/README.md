---
lang: en-US
documentclass: scrreprt
papersize: a4
fontsize: 12pt
author: Adam Štěpánek
date: 2023-05-07
...

# CodePizza

**Concept:** A pizza for every C# codebase.

**Description:** We've seen software visualizations that looked like cities, forests, islands, and galaxies. However, with the latest cutting-edge technologies, we can finally try to answer the question: _“If my code were pizza, what would it look like?”_ Thus, I propose extending the Helveg library with configurable shaders and sprites to render pizza dough, tomato sauce, and all the necessary ingredients.

**Technologies:** Helveg, Roslyn, Sigma.js, WebGL.

\clearpage

**Current state:**

![Helveg](helveg.png){width=55%}\

**Target state:**

![Pizza](pizza.jpg){width=55%}\

> [Image courtesy of pikisuperstar](https://www.freepik.com/free-vector/hand-drawn-italian-cuisine_23987088.htm#query=pizza%20illustration&position=40&from_view=keyword&track=ais)
