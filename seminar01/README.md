# Seminar 01

## ASCIIart

* Model: Bc. Adam Ivora
* Source file: ivora_original.jpg, ivora_cropped.jpg

### Attempt 1: https://www.ascii-art-generator.org/

* banner_01.txt
* banner_02.txt
* banner_color_01.svg
* banner_color_02.svg

### Attempt 2: TypePic

* letters_01.png
* letters_02.png
