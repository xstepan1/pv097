pandoc .\README.md -o tmp/README.tex `
    -s `
    -f markdown+smart
    # -M lang=en-US `
#     -M documentclass=scrreprt `
#     -M papersize=a4 `
#     -M fontsize=12pt `
#     -M mainfont=Palatino Linotype `
#     -M sansfont=Jost `
#     -M monofont=CascadiaMono
    # -M sansfontoptions={UprightFont=*-Regular, ItalicFont=*-Italic, BoldFont=*-Medium, BoldItalicFont=*-MediumItalic}`
    # -M monofontoptions:
    # - UprightFont=*-Regular
    # - ItalicFont=*-Italic
    # - BoldFont=*-Bold
    # - BoldItalicFont=*-BoldItalic

tectonic -X compile tmp/README.tex --outfmt pdf -Z search-path="$(pwd)" -Z shell-escape
