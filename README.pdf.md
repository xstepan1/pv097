---
lang: en-US
documentclass: scrreprt
papersize: a4
fontsize: 12pt
author: Adam Štěpánek
title: Visual Creativity Informatics
date: 2023-05-07
...


Hello, and welcome to my repo for the _PV097 Visual Creativity Informatics_ course.

Below, you'll find my best outputs from each seminar session I attended. If you're interested in the original parameters, look through the `seminar0*` folders. Most of them should be there.


## _Ein echter Prinz_

![Ein echter Prinz](./final/thumbs/ein_echter_prinz.jpg){width=60%}\

> Started on **Seminar 1: Letterism**.

1. Take a photo of A.I. in Stuttgart.
2. Use [ascii-art-generator](https://www.ascii-art-generator.org/)

## _Spaghetti_

![Spaghetti](./final/thumbs/spaghetti.jpg){width=100%}\

> Started during **Seminar 2: Chaotic Attractors**.

1. Make a [strange attractor](https://syntopia.github.io/StrangeAttractors/) that looks like spaghetti.
2. Crop it to a square.
3. Add a vignette.
4. Apply Photoshop's _Poster Edges_ filter.

## _Hexacore_

![Hexacore](./final/thumbs/hexacore.jpg){width=100%}\

> Started during **Seminar 3: Mosaics**.

1. Make a mosaic in MoSaiC.
2. Import it into Illustrator, shift it, and mess around with the colors.
2. Use [PhotoMosh](https://photomosh.com/) to make it cooler.

## _Golem_

![Golem](./final/thumbs/golem.jpg){width=80%}\

> Started during **Seminar 4: Decorative Knots**.

1. Make a man-shaped knot in [_dmackinnon1's_ editor](https://dmackinnon1.github.io/celtic/).
2. Export it as SVG.
3. Change the colors in Illustrator.
4. Move the body parts closer together.
5. Adjust the outlines.


## _Jabloň_

![Jabloň](./final/thumbs/jablon.jpg){width=100%}\

> Started during **Seminar 5: Fractals**.

1. Generate a tree using an L-system using _Rozmarný L-system_.
2. Export it.
3. Remove the background.
4. Put the tree on a Blender default cube, stretch the texture, and a shadow catcher plane.
5. Add an [HDRi texture](https://hdri-haven.com/hdri/meadow-at-hill).
6. Figure out what causes shadows despite the default light being disabled. Hint: it's the sky texture.
7. Rotate the whole scene to get nice shadows.
8. Position the camera.
9. Render the image and lock the screen, which causes Blender to freeze.
10. Get back from the supermarket and restart Blender.
11. Start the render but turn it off after a minute because you don't have 17 days of render time.
12. Configure the render with sane settings, i.e., 16 iterations per tile instead of 4096.
13. Save the image.
